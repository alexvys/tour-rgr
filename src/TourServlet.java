import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.http.HttpEntity;
import org.apache.http.HttpHeaders;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import com.google.gson.Gson;

@WebServlet("/tour")
public class TourServlet extends HttpServlet {
	private Gson gson = new Gson();

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		String from = request.getParameter("from");
		String to = request.getParameter("to");
		String fromId = request.getParameter("from-id");
		String toId = request.getParameter("to-id");
		String type = request.getParameter("type");
		
		CloseableHttpClient httpClient = HttpClients.createDefault();
		try {
			HttpGet req = new HttpGet(
					"https://suggest.travelpayouts.com/search?service=tutu_trains&term=" + fromId + 
					"&term2=" + toId);
			req.addHeader("accept-language", "ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7");
			req.addHeader(HttpHeaders.USER_AGENT, "Googlebot");

			CloseableHttpResponse resp = httpClient.execute(req);
			try {
				HttpEntity entity = resp.getEntity();
				if (entity != null) {
					String result = EntityUtils.toString(entity);
					Trips trips = gson.fromJson(result, Trips.class);
					List<Trip> tripsList = trips.getTrips();
					
					request.setAttribute("from", from);
					request.setAttribute("to", to);
					request.setAttribute("fromId", fromId);
					request.setAttribute("toId", toId);
					request.setAttribute("type", type);
					request.setAttribute("body", getTableBody(tripsList, type));

					getServletContext().getRequestDispatcher("/tour.jsp").forward(request, response);
				}

			} finally {
				resp.close();
			}
		} finally {
			httpClient.close();
		}
	}

	private String getTableBody(List<Trip> tripsList, String type) {
		String result = "<table>";
		result = result + "<tr><td>������������</td><td>���</td><td>����</td></tr>";
		for (int i = 0; i < tripsList.size(); i++) {
			result = result + "<tr>";
			Trip trip = tripsList.get(i);
			List<Category> filteredCategory = new ArrayList(); 
			if (!type.equals("all")) {
				for (int k = 0; k < trip.getCategories().size(); k++) {
					Category category = trip.getCategories().get(k);
					if (category.getType().equals(type))
						filteredCategory.add(category);
				}
			} else
				filteredCategory.addAll(trip.getCategories());
			int categoriesLength = filteredCategory.size();
			String name;
			if (categoriesLength != 0) {
				result = result + "<td rowspan=" + categoriesLength + ">";
				if (trip.getName() != null)
					name = trip.getName();
				else
					name = "";
				result = result + name + "</td>";
				for (int j = 0; j < filteredCategory.size(); j++) {
					Category category = filteredCategory.get(j);
					if (j != 0)
						result = result + "<tr>";
					result = result + "<td>" + category.getTypeName() + "</td><td>" + category.getPrice() + "</td>"; 
					if (j != 0)
						result = result + "</tr>";
				}
			}
			
			result = result + "</tr>";
		}
		result = result + "</table>";
		return result;
	}
}
