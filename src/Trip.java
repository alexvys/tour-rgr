
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Trip {

	@SerializedName("name")
	@Expose
	private String name;
	@SerializedName("departureStation")
	@Expose
	private String departureStation;
	@SerializedName("arrivalStation")
	@Expose
	private String arrivalStation;
	@SerializedName("runDepartureStation")
	@Expose
	private String runDepartureStation;
	@SerializedName("runArrivalStation")
	@Expose
	private String runArrivalStation;
	@SerializedName("departureTime")
	@Expose
	private String departureTime;
	@SerializedName("arrivalTime")
	@Expose
	private String arrivalTime;
	@SerializedName("trainNumber")
	@Expose
	private String trainNumber;
	@SerializedName("categories")
	@Expose
	private List<Category> categories = null;
	@SerializedName("travelTimeInSeconds")
	@Expose
	private String travelTimeInSeconds;
	@SerializedName("firm")
	@Expose
	private Boolean firm;
	@SerializedName("numberForUrl")
	@Expose
	private String numberForUrl;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDepartureStation() {
		return departureStation;
	}

	public void setDepartureStation(String departureStation) {
		this.departureStation = departureStation;
	}

	public String getArrivalStation() {
		return arrivalStation;
	}

	public void setArrivalStation(String arrivalStation) {
		this.arrivalStation = arrivalStation;
	}

	public String getRunDepartureStation() {
		return runDepartureStation;
	}

	public void setRunDepartureStation(String runDepartureStation) {
		this.runDepartureStation = runDepartureStation;
	}

	public String getRunArrivalStation() {
		return runArrivalStation;
	}

	public void setRunArrivalStation(String runArrivalStation) {
		this.runArrivalStation = runArrivalStation;
	}

	public String getDepartureTime() {
		return departureTime;
	}

	public void setDepartureTime(String departureTime) {
		this.departureTime = departureTime;
	}

	public String getArrivalTime() {
		return arrivalTime;
	}

	public void setArrivalTime(String arrivalTime) {
		this.arrivalTime = arrivalTime;
	}

	public String getTrainNumber() {
		return trainNumber;
	}

	public void setTrainNumber(String trainNumber) {
		this.trainNumber = trainNumber;
	}

	public List<Category> getCategories() {
		return categories;
	}

	public void setCategories(List<Category> categories) {
		this.categories = categories;
	}

	public String getTravelTimeInSeconds() {
		return travelTimeInSeconds;
	}

	public void setTravelTimeInSeconds(String travelTimeInSeconds) {
		this.travelTimeInSeconds = travelTimeInSeconds;
	}

	public Boolean getFirm() {
		return firm;
	}

	public void setFirm(Boolean firm) {
		this.firm = firm;
	}

	public String getNumberForUrl() {
		return numberForUrl;
	}

	public void setNumberForUrl(String numberForUrl) {
		this.numberForUrl = numberForUrl;
	}

}
