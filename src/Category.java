
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Category {
	@SerializedName("type")
	@Expose
	private String type;
	@SerializedName("price")
	@Expose
	private Integer price;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Integer getPrice() {
		return price;
	}

	public void setPrice(Integer price) {
		this.price = price;
	}

	public String getTypeName() {
		switch (type) {
		case "plazcard":
			return "��������";
		case "coupe":
			return "����";
		case "sedentary":
			return "�������";
		case "lux":
			return "����";
		case "soft":
			return "������";
		default:
			return "";
		}
	}

}
