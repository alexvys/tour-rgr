

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

@WebServlet("/stations")
public class StationServlet extends HttpServlet {
	private URL stationsUrl;
	private URL routesUrl;
	private ServletContext context;
		
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if (context == null)
			context = getServletConfig().getServletContext();
		if (stationsUrl == null)
			stationsUrl = context.getResource("/WEB-INF/db/stations.csv");
		if (routesUrl == null)
			routesUrl = context.getResource("/WEB-INF/db/tutu_routes.csv");
		BufferedReader br = null;
        String line = "";
        String cvsSplitBy = ";";
        Station station;
        String[] csvLine;
        List<Station> stationList = new ArrayList();
        
        Gson gson = new Gson();
        
        try {
        	String method = request.getParameter("method");
            if (method.equals("from")) {
                br = new BufferedReader(
                        new InputStreamReader(new FileInputStream(stationsUrl.getFile()), "UTF-8"));
                while ((line = br.readLine()) != null) {
                    csvLine = line.split(cvsSplitBy);
                    if (csvLine.length == 2) {
                    	station = new Station(csvLine[0], csvLine[1]);
                    	stationList.add(station);
                    }
                }
            } else if (method.equals("to")) {
            	String codeFrom = request.getParameter("codeFrom");
                br = new BufferedReader(
                        new InputStreamReader(new FileInputStream(routesUrl.getFile()), "UTF-8"));
                while ((line = br.readLine()) != null) {
                    csvLine = line.split(cvsSplitBy);
                    if (csvLine.length == 4 && csvLine[0].equals(codeFrom)) {
                    	station = new Station(csvLine[2], csvLine[3]);
                    	stationList.add(station);
                    }
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
		
		String responseText = gson.toJson(stationList);

	    response.setContentType("text/plain");
	    response.setCharacterEncoding("UTF-8"); 
	    response.getWriter().write(responseText);   
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse response) throws ServletException, IOException {
		String text = "some text";

	    response.setContentType("text/plain");
	    response.setCharacterEncoding("UTF-8"); 
	    response.getWriter().write(text);   
	}

}
