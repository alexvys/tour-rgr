<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<title>Расчет тура</title>
<style>
table {
	width: 100%; /* Ширина таблицы */
	border-collapse: collapse; /* Отображать только одинарные линии */
}

tr {
	padding: 5px; /* Поля вокруг содержимого ячеек */
	border: 1px solid black; /* Граница вокруг ячеек */
}

td {
	padding: 5px; /* Поля вокруг содержимого ячеек */
	border: 1px solid black; /* Граница вокруг ячеек */
}
</style>
</head>
<body>
	<p id="from-id" value="${fromId}" style="display: none" />
	<p id="to-id" value="${toId}" style="display: none" />
	<p id="type" value="${type}" style="display: none" />
	<p>Из: ${from}</p>
	<p>В: ${to}</p>
	<div id="result">
		${body}
	</div>
</body>
</html>